
+ [http://pandoc.org/scripting.html](http://pandoc.org/scripting.html)
+ [pandoc-1.19.2.1](https://hackage.haskell.org/package/pandoc-1.19.2.1/docs/Text-Pandoc.html)
+ [pandoc-types-1.19](https://hackage.haskell.org/package/pandoc-types-1.19)
    + [Text-Pandoc-Definition.html#t:Block](https://hackage.haskell.org/package/pandoc-types-1.17.0.5/docs/Text-Pandoc-Definition.html#t:Block)
    + [Text-Pandoc-Definition.html#t:Inline](https://hackage.haskell.org/package/pandoc-types-1.17.0.5/docs/Text-Pandoc-Definition.html#t:Inline)
+ [http://tech.mozilla.com.tw/posts/5306/%E6%89%93%E9%80%A0%E5%B0%88%E5%B1%AC%E6%96%BC%E4%BD%A0%E7%9A%84-git-%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%A8%8B-alias%E3%80%81commands%E3%80%81hooks](http://tech.mozilla.com.tw/posts/5306/%E6%89%93%E9%80%A0%E5%B0%88%E5%B1%AC%E6%96%BC%E4%BD%A0%E7%9A%84-git-%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%A8%8B-alias%E3%80%81commands%E3%80%81hooks)
