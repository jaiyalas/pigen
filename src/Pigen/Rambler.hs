module Pigen.Rambler where

import Data.Maybe
import Data.List

import Text.Pandoc


type BulletItem = [Block]
--
data Rambler a = Rambler (Maybe a) [Rambler a]
               | RamblerLeaf a
               deriving (Show, Eq)
--
bullet :: BulletItem -> Rambler Block
bullet [Plain ls] = RamblerLeaf (Plain ls)
bullet [Plain ls, BulletList bss] =
    Rambler (Just $ Plain ls) (fmap bullet bss)
bullet _ = Rambler Nothing []
--





--
