module Pigen.Dir where

-- import Data.Maybe
import Data.List ((\\),isSuffixOf)
import System.Directory
import Control.Monad
--
showCurrentMDs :: IO ()
showCurrentMDs =
    getCurrentMDs >>= mapM putStrLn
    >> return ()
--
showCurrentPNGs :: IO ()
showCurrentPNGs =
    getCurrentPNGs >>= mapM putStrLn
    >> return ()
--
getMDs :: FilePath -> IO [String]
getMDs = liftM (filter isMD) <=< listDirectory
--
getPNGs :: FilePath -> IO [String]
getPNGs = liftM (filter isPNG) <=< listDirectory
--
getCurrentMDs :: IO [String]
getCurrentMDs =
    getMDs =<< getCurrentDirectory
--
getCurrentPNGs :: IO [String]
getCurrentPNGs =
    getPNGs =<< getCurrentDirectory
--
getUnchartedMD :: FilePath -> IO [String]
getUnchartedMD path = do
    mds <- getMDs path
    pngs <- getPNGs path
    return (mds \\ pngs)
--
isMD :: String -> Bool
isMD x = (isSuffixOf ".md" x) ||
         (isSuffixOf ".markdown" x)
--
isPNG :: String -> Bool
isPNG = isSuffixOf ".png"
