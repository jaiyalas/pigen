# Meeting

## Date

+ A
    + a1
    + a2
+ B
    + b1

## Attendees

+ @/staff1/
+ @/staff2/

# Agenda

+ A
+ [ ][3] C
+ D
    + [ ][2] d1
    + [x][5] d2

# Discussion

+ /Stuff we actually talked about/

# Action Items

+ [ ] @someone let’s get this done

---

data BList = Title Block BList

BulletList
    [ [ Plain [ Str "A" ] ],
      [ Plain [ Str "[",
                Space,
                Str "][3]",
                Space,
                Str "C" ] ],
      [ Plain [ Str "D" ],
        BulletList [ [ Plain [ Str "[",
                               Space,
                               Str "][2]",
                               Space,
                               Str "d1"
                             ]
                     ],
                     [ Plain [ Str "[x][5]",
                               Space,
                               Str "d2"
                             ]
                     ]
                   ]
      ]
    ]


B +-- P str
  |
  +-- P str
  |
  +-- P str -- B +-- P str
                 |
                 +-- P str

data BL = Root [Block]
        | Endpoint Block
        | Fork Block [Block]

data Rambler a = Rambler (Maybe a) [Rambler a]
               | RamblerLeaf a

--
