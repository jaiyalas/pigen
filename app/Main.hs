module Main where

import Data.Maybe
import Data.List

import Text.Pandoc
import Text.Pandoc.Walk (walk, query)

import Graphics.Rendering.Chart.Plot.Pie
import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

import Data.Colour
import Data.Colour.Names

import Pigen.Dir
import Pigen.Rambler

myColorSeq :: [AlphaColour Double]
myColorSeq = cycle $ map opaque
    [ antiquewhite
    , paleturquoise
    , khaki
    , cornsilk
    , turquoise
    , palegoldenrod
    , lightcyan
    , lightskyblue
    , lightgray
    , lightcoral
    , lightgoldenrodyellow
    , lightblue
    , lightpink
    , lightgreen
    , lightseagreen
    , lightyellow
    , lightslategray
    , lightsteelblue
    ]

values = [ (PieItem (show i) 0 1) | i <- [1..40] ]

main2 = toFile def "example5_big.png" $ do
    pie_title .= "Relative Population"
    pie_plot . pie_label_style . font_size .= 20
    pie_plot . pie_colors .= myColorSeq
    pie_plot . pie_data .= values

--

main = do
    currentFile <- readFile "template.md"
    let (Pandoc meta blocks) = readDoc currentFile
    -- let counter = walk mdHandler currentPandoc
    --
    -- let res = query yolo currentPandoc
    -- putStrLn $ show res
    putStrLn $ show $ getAgenda blocks
    putStrLn "---"
    putStrLn $ show $ bullet2int $ getAgenda blocks
    -- pwd <- getCurrentDirectory
    -- ps <- listDirectory pwd
    -- let pngs = filter (isSuffixOf ".png") ps
    -- let mds  = filter (isSuffixOf ".md") ps
    -- mapM putStrLn mds
    -- putStrLn "---"
    -- mapM putStrLn pngs
    -- putStrLn "---"
    -- mapM putStrLn $ mds \\ pngs
    return ()


getAgenda :: [Block] -> Block
getAgenda ((Header _ _ [Str "Agenda"])
          :(BulletList bss)
          :bs) = BulletList bss
getAgenda (b:bs) = getAgenda bs
getAgenda [] = Null

readDoc :: String -> Pandoc
readDoc s = case readMarkdown def s of
                Right doc -> doc
                Left err  -> error (show err)

bullet2int :: Block -> [Int]
-- bullet2int (Plain ) = [1]
bullet2int (Plain (Str "[":Space:Str meta:Space:Str _:xs)) = [getWeight meta]
bullet2int (Plain (Str meta:Space:Str _:xs)) = [getWeight meta]
bullet2int (Plain _) = [1]
bullet2int (BulletList xss) =
    fmap (sum . concat . fmap bullet2int) xss


getWeight :: String -> Int
getWeight xs | isPrefixOf "[x][" xs =
    read $ takeWhile (/=']') $ drop 4 xs
getWeight xs | isPrefixOf "][" xs   =
    read $ takeWhile (/=']') $ drop 2 xs
getWeight _ = 1

-- Plain [Str "[",Space,Str "][i]",Space,Str str]
-- Plain [Str "[x][i]", Space, Str str]
-- Plain [Str str]

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

behead :: Block -> Block
behead (Header n _ xs) = Plain [Emph xs]
behead x = x

writeDoc :: Pandoc -> String
writeDoc doc = writeMarkdown def doc
